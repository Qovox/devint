# Color Hero

## Projet DeViNT 2017

## Description:
 Jeu de tir et de réflexion adapté à un public déficient visuel.

### Public visé
	Tout enfant âgé de 6 à 12 ans, ayant pour handicap soit une vision déficiente, soit une mobilité réduite.

## Team Information

  * Team name: 4
  * Members:
    *  [Olivier Boulet](mailto:olivier.boulet@etu.unice.fr)
    *  [Enzo Dalla-Nora](mailto:enzo.dalla-nora@etu.unice.fr)
    *  [Jérémy Lara](mailto:jeremy.lara@etu.unice.fr)
    *  [Alexandre Redon](mailto:alexandre.redon@etu.unice.fr)

[Nous contacter](mailto:alexandre.redon@etu.unice.fr,olivier.boulet@etu.unice.fr,jeremy.lara@etu.unice.fr,enzo.dalla-nora@etu.unice.fr)

    