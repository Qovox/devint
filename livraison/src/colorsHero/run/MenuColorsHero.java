package colorsHero.run;

import colorsHero.jeu.loader.Launcher;
import dvt.devint.menu.MenuDevint;
import javafx.application.Application;

import static colorsHero.jeu.loader.Launcher.*;

public class MenuColorsHero extends MenuDevint {


    @Override
    public String titre() {
        return "Colors Hero";
    }

    @Override
    public void initMenu() {
        control.addMenuItem("Règles & Touches", x -> Launcher.launch(LAUNCH_COMMAND));
        control.addMenuItem("Jouer", x -> launch());
        control.addMenuItem("Panneau administrateur", x -> Launcher.launch(LAUNCH_ADMIN_PANEL));
    }

    private void launch() {
        Launcher.reset();
        Launcher.launch(LAUNCH_GAME);
    }

    public static void main(String[] s) {
        Application.launch(MenuColorsHero.class, s);
    }

}
