package colorsHero.jeu.command;

import dvt.jeu.simple.ControleDevint;

public class CommandController extends ControleDevint {

    /**
     * methode appelee avant la boucle de jeu
     * utilisee pour l'itialisation du modèle et de la vue
     */
    @Override
    protected void init() {

    }

    /**
     * methode appelee quand on active la touche pour re-commencer le jeu
     * NOTA : si votre jeu se joue à l'infini, redéfinir cette fonction
     * en ne faisant rien
     */
    @Override
    protected void reset() {

    }

    /**
     * méthode appelée pour associer les actions aux touches dans la scene devint
     * est un ensemble d'instructions de la forme
     * scene.mapKeyPressedToConsumer(KeyCode.<touche>, (x) -> <fonction>);
     * où <touche> est une touche du clavier et <fonction> est un appel de fonction du contrôle
     */
    @Override
    public void mapTouchToActions() {

    }

}
