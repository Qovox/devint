package colorsHero.jeu.game.managers;

import colorsHero.jeu.game.CsteJeu;
import colorsHero.jeu.game.entity.Monster;
import colorsHero.jeu.game.entity.NatureType;
import colorsHero.jeu.game.entity.Player;
import colorsHero.jeu.game.entity.Weapon;
import colorsHero.jeu.score.ScoreManager;

import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Enzo on 21/05/2017.
 */
public class BossMinionManager extends MonsterManager {

    private List<Monster> monsterList;

    public BossMinionManager() {
        super();
        this.monsterList = new ArrayList<>();
    }

    /**
     * Method that update positions of the monsters
     */
    public void updateMonstersPositions(Player player, double height) {
        Iterator<Monster> monsterIterator = monsterList.iterator();

        while (monsterIterator.hasNext()) {
            Monster monster = monsterIterator.next();
            monster.moveStraight(-CsteJeu.MONSTERSPEED);

            if (monster.isOutOfScreen()) {
                monster.tookDamage(monster.getNatureType());
                player.tookDamage((float) 0.5);
                ScoreManager.score
                        += (CsteJeu.scorePerHit);
            }
        }
    }

    public boolean spawnMonsters(String monsterSprite) {
        long deltaTime = System.currentTimeMillis() - spawnTimeCount;
        timerSpawn += deltaTime;

        if (timerSpawn > CsteJeu.MONSTERSPAWNINTERVAL) {
            resetTimer();

            addRandomMonster(monsterSprite);

            return true;
        }
        return false;
    }

    private void addRandomMonster(String monsterSprite) {
        Monster monster = new Monster(new Weapon(1, NatureType.THUNDER), new Point(0, -1));
        monster.initEntitySprite(monsterSprite);

        monsterList.add(monster);
    }

    public void visualMonstersUpdate(BoardManager boardManager, Player player) {
        Iterator<Monster> monsterIterator = monsterList.iterator();

        while (monsterIterator.hasNext()) {
            Monster monster = monsterIterator.next();

            if (monster.isInstantiated()) {
                int playerHeight = (int) (player.getImage().getImage().getHeight() / 2);
                int randPos = ThreadLocalRandom.current().nextInt(playerHeight, (int) boardManager.getHeightScreen() - playerHeight + 1);

                monster.setLocation(new Point((int) boardManager.getWidthScreen(), randPos));

                boardManager.addEntityToCanvas(monster);
            }

            monster.refreshSpritePosition();
        }
    }

    /**
     * Methode qui permet de supprimer du jeu tous les monstres
     * qui ont été tué par le joueur
     *
     * @param boardManager le boardmanager du jeu
     */
    public void deleteMonstersFromGame(BoardManager boardManager) {
        Iterator<Monster> monsterIterator = monsterList.iterator();

        while (monsterIterator.hasNext()) {
            Monster monster = monsterIterator.next();

            if (monster.isDead()) {
                boardManager.removeEntityFromCanvas(monster);
                monsterIterator.remove();
            }
        }
    }

    /**
     * Methode qui permet de supprimer du jeu tous les monstres
     * qui ont été tué par le joueur
     *
     * @param boardManager le boardmanager du jeu
     */
    public boolean deleteMinionsFromGame(BoardManager boardManager) {
        Iterator<Monster> monsterIterator = monsterList.iterator();

        boolean res = false;

        while (monsterIterator.hasNext()) {
            Monster monster = monsterIterator.next();

            if (monster.isDead()) {
                boardManager.removeEntityFromCanvas(monster);
                monsterIterator.remove();
                res = true;
            }
        }
        return res;
    }

    public void flushMonstersFromGame(BoardManager boardManager) {
        boardManager.clearAllMonsters(monsterList);
        monsterList = new ArrayList<>();
    }

    public List<Monster> getMonsterList() {
        return monsterList;
    }

    public void reset(BoardManager boardManager) {
        this.flushMonstersFromGame(boardManager);
        resetTimer();
    }

}
