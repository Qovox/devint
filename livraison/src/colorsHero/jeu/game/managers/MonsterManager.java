package colorsHero.jeu.game.managers;

import colorsHero.jeu.game.entity.Player;

import java.util.Observable;

/**
 * Created by Enzo on 01/04/2017.
 */
public abstract class MonsterManager extends Observable {

    protected long spawnTimeCount;
    protected long timerSpawn;

    /**
     * MonsterManager constructor
     */
    public MonsterManager() {
        resetTimer();
    }

    /**
     * Methode permettant de remettre a 0 le timer utilisé pour l'apparition des ennemis
     */
    protected void resetTimer() {
        this.timerSpawn = 0;
        this.spawnTimeCount = System.currentTimeMillis();
    }

    public abstract void updateMonstersPositions(Player player, double height);

    public abstract void flushMonstersFromGame(BoardManager boardManager);

    public abstract void deleteMonstersFromGame(BoardManager boardManager);

    public abstract void visualMonstersUpdate(BoardManager boardManager, Player player);

    public abstract void reset(BoardManager boardManager);

}
