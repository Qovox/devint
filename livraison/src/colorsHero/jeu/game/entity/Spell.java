package colorsHero.jeu.game.entity;

import colorsHero.jeu.game.CsteJeu;
import javafx.scene.shape.Rectangle;

import java.awt.*;

/**
 * Created by Enzo on 31/03/2017.
 */
public class Spell {

    private Point position;
    private NatureType natureType;
    private Rectangle rectangle;
    private boolean mustBeRemoved;

    /**
     * Spell constructor
     *
     * @param position   the initial position of the spell
     * @param natureType the nature type of the spell
     */
    public Spell(Point position, NatureType natureType) {
        this.position = position;
        this.natureType = natureType;
        rectangle = new Rectangle();
        mustBeRemoved = false;
    }

    /**
     * Initialise the rectangle of the spell
     */
    public void initRectangle() {
        rectangle.setWidth(CsteJeu.WIDTHSPELL);
        rectangle.setHeight(CsteJeu.HEIGHTSPELL);
        rectangle.setFill(natureType.getColor());
        rectangle.setLayoutY(position.y);
    }

    /**
     * Spell position getter
     *
     * @return the position of the spell
     */
    public Point getPosition() {
        return position;
    }

    /**
     * Spell nature type getter
     *
     * @return the nature type of the spell
     */
    public NatureType getNatureType() {
        return natureType;
    }

    /**
     * Method that actualise the position of the spell by a fixed distance, defined in the CsteJeu class
     */
    public void moveStraight() {
        position.x += CsteJeu.SPELLSPEED;
    }

    public void visualMoveStraight() {
        rectangle.setX(position.x);
    }

    /**
     * Method that indicate if the spell is out of the screen, depending of it position and the size of the screen
     *
     * @param sizeScreen the size of the screen
     * @return true if the spell is out of the screen. False otherwise
     */
    public boolean isOutOfScreen(double sizeScreen) {
        return position.x < 0 || position.x >= sizeScreen;
    }

    /**
     * Spell rectangle getter
     *
     * @return the rectangle of the spell
     */
    public Rectangle getRectangle() {
        return rectangle;
    }

    public void setMustBeRemoved(boolean mustBeRemoved) {
        this.mustBeRemoved = mustBeRemoved;
    }

    public boolean getMustBeRemoved() {
        return mustBeRemoved;
    }

}
