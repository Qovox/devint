package colorsHero.jeu.game.entity;

public class Weapon {

    private int damage;
    private NatureType type;

    /**
     * Weapon constructor
     *
     * @param damage the damages the weapon inflicts
     * @param type   the weapon's damages type
     */
    public Weapon(int damage, NatureType type) {
        this.damage = damage;
        this.type = type;
    }

    /**
     * Weapon damage getter
     *
     * @return the damage of the weapon
     */
    public int getDamage() {
        return damage;
    }

    /**
     * Weapon nature type getter
     *
     * @return the nature type of the weapon
     */
    public NatureType getNatureType() {
        return type;
    }

    public void setNatureType(NatureType natureType) {
        this.type = natureType;
    }

}
