package colorsHero.jeu.game.entity;

import colorsHero.jeu.game.CsteJeu;
import colorsHero.jeu.score.ScoreManager;
import dvt.devint.ConstantesDevint;
import javafx.geometry.Bounds;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import java.awt.*;

public class Entity {

    protected float life;
    protected Weapon weapon;
    protected NatureType natureType;
    protected Point topPosition;
    private Rectangle rectangle;
    protected ImageView image;

    /**
     * Entity constructor
     *
     * @param weapon   the initial weapon of the entity
     * @param position the initial position of the entity
     */
    public Entity(Weapon weapon, Point position) {
        this.weapon = weapon;
        this.topPosition = position;
        image = new ImageView();
        rectangle = new Rectangle();
    }


    /**
     * Method that initialise the graphic part of the entity
     *
     * @param imagePath the path to find the image of the entity
     */
    public void initEntitySprite(String imagePath) {
        Image tmp = new Image(ConstantesDevint.ressourcesFileName(imagePath));
        image.setImage(tmp);
        image.setLayoutX(topPosition.x);
        image.setLayoutY(topPosition.y);

        double r = natureType.getColor().getRed();
        double g = natureType.getColor().getGreen();
        double b = natureType.getColor().getBlue();

        Color color = new Color(r, g, b, CsteJeu.FILTERTRANSPARENCY);

        rectangle.setX(topPosition.x);
        rectangle.setY(topPosition.y);
        rectangle.setWidth(tmp.getWidth());
        rectangle.setHeight(tmp.getHeight());
        rectangle.setFill(color);
    }

    /**
     * Method that actualise the health of the entity
     *
     * @param dmg the damage took by the entity
     */
    public void tookDamage(float dmg) {
        life -= dmg;
    }

    /**
     * Method that set the life of the entity to 0 if the incoming attack is the same
     * type as the entity's type
     *
     * @param natureType the type of the incoming attack
     */
    public void tookDamage(NatureType natureType) {
        if (natureType == this.natureType) {
            life = 0;
            ScoreManager.score += ((CsteJeu.GAMEDIFFICULTY.getDiff() + 1) * CsteJeu.scoreMultiplier * CsteJeu.scorePerKill);
        }
    }

    /**
     * Method that return true if the life of the entity is equal or below 0
     *
     * @return true if the entity is dead. False otherwise
     */
    public boolean isDead() {
        return life <= 0;
    }

    /**
     * Method that return if a point collided with the entity
     *
     * @param bounds the bounds we want to know if it touch the entity
     * @return true if the point collided with the entity. False otherwise
     */
    public boolean isHit(Bounds bounds) {
        Bounds currentBounds = rectangle.localToScene(rectangle.getBoundsInLocal());

        return currentBounds.intersects(bounds);
    }

    /**
     * Method that move the player up, by a fixed amount defined in the CsteJeu class
     */
    public void moveUp() {
        this.topPosition.y -= CsteJeu.MOVESPLAYER;
        refreshSpritePosition();
    }

    /**
     * Method that move the player down, by a fixed amount defined in the CsteJeu class
     */
    public void moveDown() {
        this.topPosition.y += CsteJeu.MOVESPLAYER;
        refreshSpritePosition();
    }

    /**
     * Method that actualise the position of the image of the entity
     * whenever it is needed
     */
    public void refreshSpritePosition() {
        image.setLayoutY(topPosition.y);
        image.setLayoutX(topPosition.x);
        rectangle.setY(topPosition.y);
        rectangle.setX(topPosition.x);
    }

    public void moveStraight(int move) {
        topPosition.x += move;
    }

    /**
     * Entity imageview getter
     *
     * @return the imageview of the entity
     */
    public ImageView getImage() {
        return image;
    }

    /**
     * Entity top position getter
     *
     * @return the top position of the entity
     */
    public Point getTopPosition() {
        return topPosition;
    }


    /**
     * Method that return whether the entity is out of the screen
     *
     * @param height the current screen's height
     * @return true if the entity is out of the screen. False otherwise
     */
    public boolean isUnderGround(double height) {
        return (topPosition.y + image.getImage().getHeight()) >= height;
    }

    /**
     * Entity nature type getter
     *
     * @return the nature type of the entity
     */
    public NatureType getNatureType() {
        return natureType;
    }

    /**
     * Entity weapon's nature type getter
     *
     * @return the weapon's nature type of the entity
     */
    public NatureType getWeaponNatureType() {
        return weapon.getNatureType();
    }

    /**
     * Entity rectangle getter
     *
     * @return the rectangle of the entity
     */
    public Rectangle getRectangle() {
        return rectangle;
    }

    public boolean isOutOfScreen(double sizeScreen) {
        return topPosition.x < 0 || topPosition.x >= sizeScreen;
    }

    public void setLocation(Point location) {
        this.topPosition = location;
    }

    public float getLife() {
        return life;
    }

}