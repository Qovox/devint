package colorsHero.jeu.game;

public class CsteJeu {

    public enum DIFFICULTY {

        EASY(0, 7),
        NORMAL(1, 10),
        HARD(2, 13);

        private int waveLenght;
        private int diff;

        DIFFICULTY(int diff, int waveLenght) {
            this.diff = diff;
            this.waveLenght = waveLenght;
        }

        public int getDiff() {
            return diff;
        }

        public int getWaveLenght() {
            return waveLenght;
        }
    }

    public static boolean isBoss = false;
    public static final long BOSSCOLORSWITCH = 2000000;
    public static int BOSSLIFE = 15;
    public static int BOSSSPEED = 1;

    public static int PLAYERLIFE = 3;
    public static int MOVESPLAYER = 10;
    public static int playerFireTimer = 10000;

    public static int MONSTERLIFE = 3;
    public static int MONSTERSPEED = 1;
    public static int MONSTERSPAWNINTERVAL = 300000;

    public static int SPELLSPEED = 8;
    public static int WIDTHSPELL = 100;
    public static int HEIGHTSPELL = 20;

    public static double FILTERTRANSPARENCY = 0.5;
    public static DIFFICULTY GAMEDIFFICULTY = DIFFICULTY.EASY;

    public static int scoreMultiplier = 1;
    public static int scorePerKill = 100;
    public static int scorePerHit = -50;
    public static int scorePerAnswer = 300;
    public static int scoreBoss = 1000;

    public static DIFFICULTY getGameDifficulty() {
        return GAMEDIFFICULTY;
    }

    public static void setGameDifficulty(DIFFICULTY GAMEDIFFICULTY) {
        CsteJeu.GAMEDIFFICULTY = GAMEDIFFICULTY;
    }

}
