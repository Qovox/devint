package colorsHero.jeu.loader;

import dvt.jeu.simple.ControleDevint;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import static colorsHero.jeu.loader.Launcher.LAUNCH_GAME;

/**
 * Created by Enzo on 22/05/2017.
 */
public class GameOverController extends ControleDevint {
    @FXML
    private Button retryBtn;

    @FXML
    private Button stopBtn;

    private int selectedOption = 0;

    @Override
    protected void init() {
        selectButton(stopBtn);
        readText(selectedOptionText());
    }

    @Override
    protected void reset() {

    }

    @Override
    public void mapTouchToActions() {
        scene.mapKeyPressedToConsumer(KeyCode.UP, x -> up());
        scene.mapKeyPressedToConsumer(KeyCode.DOWN, x -> up());
        scene.mapKeyPressedToConsumer(KeyCode.ENTER, x -> validate());
        scene.mapKeyPressedToConsumer(KeyCode.SPACE, x -> validate());
    }

    private void validate() {
        switch (selectedOption) {
            case 0:
                quitGame();
                break;
            case 1:
                launchGame();
                break;
            default:
                break;
        }
    }

    private void launchGame() {
        Launcher.reset();
        Launcher.launch(LAUNCH_GAME);
    }

    private void quitGame() {
        Launcher.close();

    }

    /**
     * Change l'apparence de la réponse actuellement sélectionnée
     *
     * @param selectedButton le réponse sélectionnée
     */
    private void selectButton(Button selectedButton) {
        selectedButton.getStyleClass().clear();
        selectedButton.getStyleClass().add("selectedbutton");
    }

    private void up() {
        if (selectedOption == 0) {
            unSelectButton(stopBtn);
            selectedOption = 1;
            selectButton(retryBtn);
        } else if (selectedOption == 1) {
            unSelectButton(retryBtn);
            selectedOption = 0;
            selectButton(stopBtn);
        }
        readText(selectedOptionText());
    }

    private String selectedOptionText() {
        switch (selectedOption) {
            case 0:
                return "Quitter";
            case 1:
                return "Rejouer";
            default:
                return "BUG";
        }
    }

    /**
     * Change l'apparence de la question désélectionnée
     *
     * @param unselectedButton le réponse désélectionnée
     */
    private void unSelectButton(Button unselectedButton) {
        unselectedButton.getStyleClass().clear();
        unselectedButton.getStyleClass().add("unselectedbutton");
    }

    /**
     * Lit le texte passé en paramètre avec VocalyzeSIVOX.
     *
     * @param textToRead le texte à lire.
     */
    private void readText(String textToRead) {
        scene.getSIVox().stop();
        scene.getSIVox().playText(textToRead);
    }

    public void stopGame(KeyEvent keyEvent) {
        quitGame();
    }

    public void retryGame(KeyEvent keyEvent) {
        launchGame();
    }

    public void stopGame(ActionEvent keyEvent) {
        quitGame();
    }

    public void retryGame(ActionEvent keyEvent) {
        launchGame();
    }

}
