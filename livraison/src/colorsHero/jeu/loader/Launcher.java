package colorsHero.jeu.loader;

import colorsHero.jeu.game.CsteJeu;
import colorsHero.jeu.game.GameModel;
import colorsHero.jeu.qcm.QCMModel;
import colorsHero.jeu.score.ScoreManager;
import com.sun.javafx.stage.StageHelper;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import org.json.simple.parser.ParseException;

import java.io.IOException;

import static colorsHero.jeu.game.CsteJeu.GAMEDIFFICULTY;

public class Launcher {

    public static final int LAUNCH_GAME = 0;
    public static final int LAUNCH_COMMAND = 1;
    public static final int LAUNCH_ADMIN_PANEL = 2;

    //Launcher management useful static attributes
    private static final String QCM_MODEL_INIT = "../ressources/questions/questions.json";
    public static final int WAVE_NB = 4;

    private static LoaderDevintSimple ldSimple;
    private static LoaderDevintAnimation ldAnimation;

    public static int wave = 1;
    public static boolean isChoosingDifficulty = true;
    public static boolean shootPhase = false;
    public static boolean questionsPhase = false;
    public static boolean isOver = false;

    private Launcher() {
    }

    /**
     * Décide en fonction de sceneToLaunch quel scène doit être instanciée.
     *
     * @param sceneToLaunch spécifie quelle scène doit être lancée.
     */
    public static void launch(int sceneToLaunch) {
        switch (sceneToLaunch) {
            case LAUNCH_GAME:
                launchGame();
                break;
            case LAUNCH_COMMAND:
                launchCommand();
                break;
            case LAUNCH_ADMIN_PANEL:
                launchAdminPanel();
                break;
            default:
        }
    }

    /**
     * Lance le jeu et s'occupe du bon enchaînement des différentes instances de jeu.
     */
    private static void launchGame() {
        if (isOver) {
            ldAnimation.close();
            questionsPhase = false;
            ldSimple = new LoaderDevintSimple("Écran Défaite",
                    "menu/menuGameover.fxml", new VBox(), null);

        }
        if (wave > (WAVE_NB)) {

            ldAnimation.close();
            questionsPhase = false;
            ldSimple = new LoaderDevintSimple("Écran Victoire",
                    "menu/menuWin.fxml", new VBox(), null);

        }
        if (isChoosingDifficulty)
            ldSimple = new LoaderDevintSimple("Séléction difficulté",
                    "menu/menuDifficulty.fxml", new VBox(), null);
        else if (shootPhase) {
            ldSimple.close();
            ldAnimation = new LoaderDevintAnimation("Phase de Tir",
                    "shoot/shooter.fxml", new VBox(), new GameModel());
            wave++;
            shootPhase = false;
        } else if (questionsPhase) {
            ldAnimation.close();
            try {
                ldSimple = new LoaderDevintSimple("Phase de Questions", difficultyPath(), new AnchorPane(), new QCMModel(QCM_MODEL_INIT));
            } catch (IOException | ParseException e) {
                e.printStackTrace();
            }
            questionsPhase = false;
        }
    }

    /**
     * Détermine le bon chemin vers le fichier fxml approprié
     * selon la difficulté choisie par le joueur.
     *
     * @return le chemin du fichier fxml.
     */
    private static String difficultyPath() {
        switch (GAMEDIFFICULTY) {
            case NORMAL:
                return "questions/qcmNormal.fxml";
            case HARD:
                return "questions/qcmHard.fxml";
            default:
                return "questions/qcmEasy.fxml";
        }
    }

    private static void launchCommand() {
        new LoaderDevintSimple("Règles & Touches", "menu/menuCommand.fxml", new AnchorPane(), null);
    }

    private static void launchAdminPanel() {
        new LoaderDevintSimple("Panneau Administrateur", "questions/AdminPanel.fxml", new VBox(), null);
    }

    public static void reset() {
        isChoosingDifficulty = true;
        shootPhase = false;
        questionsPhase = false;
        isOver = false;
        wave = 1;
        ScoreManager.score = 0;
        CsteJeu.scoreMultiplier = 1;

        if (StageHelper.getStages().contains(ldSimple)) {
            ldSimple.close();
        }
        if (StageHelper.getStages().contains(ldAnimation)) {
            ldAnimation.close();
        }
    }

    public static void close() {
        ldAnimation.close();
        ldSimple.close();
    }

}
