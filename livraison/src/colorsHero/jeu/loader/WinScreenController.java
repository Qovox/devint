package colorsHero.jeu.loader;

import colorsHero.jeu.score.BestScore;
import colorsHero.jeu.score.ScoreManager;
import colorsHero.tools.TextHelper;
import dvt.jeu.simple.ControleDevint;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import java.util.List;

import static colorsHero.jeu.loader.Launcher.LAUNCH_GAME;

/**
 * Created by Enzo on 22/05/2017.
 */
public class WinScreenController extends ControleDevint {

    @FXML
    private Button retryBtn;

    @FXML
    private Button stopBtn;

    @FXML
    private Label highScoreLabel;

    @FXML
    private Label firstScore;

    @FXML
    private Label secondScore;

    @FXML
    private Label thirdScore;

    @FXML
    private TextField playerName;

    private int selectedOption = 0;

    private ScoreManager scoreManager;

    @Override
    protected void init() {
        scoreManager = new ScoreManager();

        selectButton(stopBtn);
        displayHighScore();
        readText(selectedOptionText());
    }

    private void displayHighScore() {
        highScoreLabel.setText("Score : " + ScoreManager.score);

        List<BestScore> tmp = scoreManager.getBestScoreList();

        firstScore.setText(tmp.get(0).toString());
        secondScore.setText(tmp.get(1).toString());
        thirdScore.setText(tmp.get(2).toString());
    }

    @Override
    protected void reset() {

    }

    @Override
    public void mapTouchToActions() {
        scene.mapKeyPressedToConsumer(KeyCode.BACK_SPACE, x -> TextHelper.backSpace(playerName));
        scene.mapKeyPressedToConsumer(KeyCode.LEFT, x -> TextHelper.caretLeft(playerName));
        scene.mapKeyPressedToConsumer(KeyCode.RIGHT, x -> TextHelper.caretRight(playerName));
        scene.mapKeyPressedToConsumer(KeyCode.UP, x -> up());
        scene.mapKeyPressedToConsumer(KeyCode.DOWN, x -> up());
        scene.mapKeyPressedToConsumer(KeyCode.ENTER, x -> validate());
    }

    @FXML
    private void checkPseudoLength(KeyEvent event) {
        if (playerName.getText().length() == 12)
            event.consume();
    }

    private void validate() {
        switch (selectedOption) {
            case 0:
                quitGame();
                break;
            case 1:
                launchGame();
                break;
            default:
                break;
        }
    }

    private void launchGame() {
        updateHighScore();
        Launcher.reset();
        Launcher.launch(LAUNCH_GAME);
    }

    private void quitGame() {
        updateHighScore();

        Launcher.close();
    }

    private void updateHighScore() {
        if (scoreManager.isNewBestScore() && !playerName.getText().isEmpty()) {
            scoreManager.rewriteBestScores(playerName.getText());
        }
    }

    /**
     * Change l'apparence de la réponse actuellement sélectionnée
     *
     * @param selectedButton le réponse sélectionnée
     */
    private void selectButton(Button selectedButton) {
        selectedButton.getStyleClass().clear();
        selectedButton.getStyleClass().add("selectedbutton");
    }

    private void up() {
        if (selectedOption == 0) {
            unSelectButton(stopBtn);
            selectedOption = 1;
            selectButton(retryBtn);
        } else if (selectedOption == 1) {
            unSelectButton(retryBtn);
            selectedOption = 0;
            selectButton(stopBtn);
        }
        readText(selectedOptionText());
    }

    private String selectedOptionText() {
        switch (selectedOption) {
            case 0:
                return "Quitter";
            case 1:
                return "Rejouer";
            default:
                return "BUG";
        }
    }

    /**
     * Change l'apparence de la question désélectionnée
     *
     * @param unselectedButton le réponse désélectionnée
     */
    private void unSelectButton(Button unselectedButton) {
        unselectedButton.getStyleClass().clear();
        unselectedButton.getStyleClass().add("unselectedbutton");
    }

    /**
     * Lit le texte passé en paramètre avec VocalyzeSIVOX.
     *
     * @param textToRead le texte à lire.
     */
    private void readText(String textToRead) {
        scene.getSIVox().stop();
        scene.getSIVox().playText(textToRead);
    }

    public void stopGame(KeyEvent keyEvent) {
        quitGame();
    }

    public void retryGame(KeyEvent keyEvent) {
        launchGame();
    }

    public void stopGame(ActionEvent keyEvent) {
        quitGame();
    }

    public void retryGame(ActionEvent keyEvent) {
        launchGame();
    }

}
