package colorsHero.jeu.loader;

import dvt.devint.ConstantesDevint;
import dvt.devint.SceneDevint;
import dvt.jeu.animation.ControleAnimationDevint;
import dvt.jeu.animation.JeuAnimationDevint;
import dvt.jeu.animation.ModeleAnimationDevint;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class LoaderDevintAnimation extends JeuAnimationDevint {

    /**
     * Constructeur de la classe LoaderDevintSimple. Permet de passer les
     * paramètres nécéssaires pour une instancier un jeu DevintAnimation.
     *
     * @param title     le titre de la scène Devint
     * @param fxml_file le fichier fxml qui constituera le layout de la scène Devint
     * @param parent    l'objet parent du layout fxml.
     */
    public LoaderDevintAnimation(String title, String fxml_file, Parent parent, ModeleAnimationDevint model) {
        super(title, fxml_file, parent, model);
    }

    /**
     * le titre du jeu
     *
     * @return le titre
     */
    @Override
    public String titre() {
        return title;
    }

    /**
     * initialisation du modèle
     * méthode appelée dans le constructeur
     */
    @Override
    protected ModeleAnimationDevint initModel() {
        if (model == null)
            return new ModeleAnimationDevint() {
                @Override
                protected void update() {
                }
            };
        return model;
    }

    /**
     * initialisation du contrôle devint et de la scene devint
     * méthode appelée dans le constructeur
     * la scene peut être construite à partir du FXML via un FXMLLoader()
     * <p>
     * on peut aussi tout créer à la main si on ne veut pas passer par FXML
     */
    @Override
    protected ControleAnimationDevint initControlAndScene() {
        SceneDevint scene;
        FXMLLoader loader = new FXMLLoader();
        try {
            loader.setLocation(new URL(fxml_file));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        try {
            parent = loader.load();
            scene = new SceneDevint(parent, ConstantesDevint.MAX_SCREEN_WIDTH, ConstantesDevint.MAX_SCREEN_HEIGHT);
            control = loader.getController();
            control.setScene(scene);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return control;
    }

}
