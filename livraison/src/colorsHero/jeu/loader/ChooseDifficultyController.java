package colorsHero.jeu.loader;

import colorsHero.jeu.game.CsteJeu;
import dvt.jeu.simple.ControleDevint;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.input.KeyCode;

import static colorsHero.jeu.loader.Launcher.*;

public class ChooseDifficultyController extends ControleDevint {

    private int selectedDifficulty = 0;

    @FXML
    private Button easyBtn;

    @FXML
    private Button normalBtn;

    @FXML
    private Button hardBtn;

    /**
     * methode appelee avant la boucle de jeu
     * utilisee pour l'itialisation du modèle et de la vue
     */
    @Override
    protected void init() {
        selectButton(easyBtn);
        readText(selectedDifficultyText());
    }

    /**
     * methode appelee quand on active la touche pour re-commencer le jeu
     * NOTA : si votre jeu se joue à l'infini, redéfinir cette fonction
     * en ne faisant rien
     */
    @Override
    protected void reset() {

    }

    /**
     * méthode appelée pour associer les actions aux touches dans la scene devint
     * est un ensemble d'instructions de la forme
     * scene.mapKeyPressedToConsumer(KeyCode.<touche>, (x) -> <fonction>);
     * où <touche> est une touche du clavier et <fonction> est un appel de fonction du contrôle
     */
    @Override
    public void mapTouchToActions() {
        scene.mapKeyPressedToConsumer(KeyCode.UP, x -> up());
        scene.mapKeyPressedToConsumer(KeyCode.DOWN, x -> down());
        scene.mapKeyPressedToConsumer(KeyCode.ENTER, x -> validate());
        scene.mapKeyPressedToConsumer(KeyCode.SPACE, x -> validate());
    }

    private void validate() {
        switch (selectedDifficulty) {
            case 0:
                setDifficulty(CsteJeu.DIFFICULTY.EASY);
                break;
            case 1:
                setDifficulty(CsteJeu.DIFFICULTY.NORMAL);
                break;
            case 2:
                setDifficulty(CsteJeu.DIFFICULTY.HARD);
                break;
            default:
                break;
        }
    }

    @FXML
    void setEasyDifficulty(ActionEvent event) {
        setDifficulty(CsteJeu.DIFFICULTY.EASY);
    }

    @FXML
    void setNormalDifficulty(ActionEvent event) {
        setDifficulty(CsteJeu.DIFFICULTY.NORMAL);
    }

    @FXML
    void setHardDifficulty(ActionEvent event) {
        setDifficulty(CsteJeu.DIFFICULTY.HARD);
    }

    private void setDifficulty(CsteJeu.DIFFICULTY difficulty) {
        isChoosingDifficulty = false;
        shootPhase = true;
        CsteJeu.setGameDifficulty(difficulty);
        launch(LAUNCH_GAME);
    }

    private void up() {
        if (selectedDifficulty == 0) {
            unSelectButton(easyBtn);
            selectedDifficulty = 2;
            selectButton(hardBtn);
        } else if (selectedDifficulty == 1) {
            unSelectButton(normalBtn);
            selectedDifficulty = 0;
            selectButton(easyBtn);
        } else if (selectedDifficulty == 2) {
            unSelectButton(hardBtn);
            selectedDifficulty = 1;
            selectButton(normalBtn);
        }
        readText(selectedDifficultyText());
    }

    private void down() {
        if (selectedDifficulty == 0) {
            unSelectButton(easyBtn);
            selectedDifficulty = 1;
            selectButton(normalBtn);
        } else if (selectedDifficulty == 1) {
            unSelectButton(normalBtn);
            selectedDifficulty = 2;
            selectButton(hardBtn);
        } else if (selectedDifficulty == 2) {
            unSelectButton(hardBtn);
            selectedDifficulty = 0;
            selectButton(easyBtn);
        }
        readText(selectedDifficultyText());
    }

    private String selectedDifficultyText() {
        switch (selectedDifficulty) {
            case 0:
                return "Facile";
            case 1:
                return "Normal";
            case 2:
                return "Difficile";
            default:
                return "BUG";
        }
    }

    /**
     * Change l'apparence de la réponse actuellement sélectionnée
     *
     * @param selectedButton le réponse sélectionnée
     */
    private void selectButton(Button selectedButton) {
        selectedButton.getStyleClass().clear();
        selectedButton.getStyleClass().add("selectedbutton");
    }

    /**
     * Change l'apparence de la question désélectionnée
     *
     * @param unselectedButton le réponse désélectionnée
     */
    private void unSelectButton(Button unselectedButton) {
        unselectedButton.getStyleClass().clear();
        unselectedButton.getStyleClass().add("unselectedbutton");
    }

    /**
     * Lit le texte passé en paramètre avec VocalyzeSIVOX.
     *
     * @param textToRead le texte à lire.
     */
    private void readText(String textToRead) {
        scene.getSIVox().stop();
        scene.getSIVox().playText(textToRead);
    }

}
