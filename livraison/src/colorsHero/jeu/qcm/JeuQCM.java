package colorsHero.jeu.qcm;

import dvt.devint.ConstantesDevint;
import dvt.devint.SceneDevint;
import dvt.jeu.simple.ControleDevint;
import dvt.jeu.simple.JeuDevint;
import dvt.jeu.simple.ModeleDevint;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.VBox;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import static colorsHero.jeu.game.CsteJeu.GAMEDIFFICULTY;

//TODO Supprimer cette classe avant la livraison finale

/**
 * Classe de jeu devint temporaire qui sert a instancié la partie
 * qcm du jeu pour l'utiliser de façon indépendante. Dans la version
 * finale, le LoaderDevintSimple se chargera de faire le boulot de cette classe.
 */
public class JeuQCM extends JeuDevint {

    @Override
    public String titre() {
        return "QCM";
    }

    @Override
    protected ModeleDevint initModel() {
        QCMModel model = null;
        try {
            model = new QCMModel("../ressources/questions/questions.json");
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        return model;
    }

    @Override
    protected ControleDevint initControlAndScene() {
        SceneDevint scene;
        FXMLLoader loader = new FXMLLoader();
        String FXMLfileName = ConstantesDevint.ressourcesFileName(difficultyPath());
        try {
            loader.setLocation(new URL(FXMLfileName));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        try {
            VBox root = (VBox) loader.load();
            scene = new SceneDevint(root, ConstantesDevint.MAX_SCREEN_WIDTH, ConstantesDevint.MAX_SCREEN_HEIGHT);
            control = loader.getController();
            control.setScene(scene);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return control;
    }

    /**
     * Détermine le bon chemin vers le fichier fxml approprié
     * selon la difficulté choisie par le joueur.
     *
     * @return le chemin du fichier fxml.
     */
    private static String difficultyPath() {
        switch (GAMEDIFFICULTY) {
            case NORMAL:
                return "questions/qcmNormal.fxml";
            case HARD:
                return "questions/qcmHard.fxml";
            default:
                return "questions/qcmEasy.fxml";
        }
    }

}
