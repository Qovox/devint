package colorsHero.jeu.qcm;

import javafx.application.Application;
import javafx.stage.Stage;

//TODO Supprimer cette classe avant la livraison finale

/**
 * MainClass temporaire pour tester la partie qcm du jeu de manière indépendante
 */
public class MainQCM extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        new JeuQCM();
    }

}
