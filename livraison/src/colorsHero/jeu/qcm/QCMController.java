package colorsHero.jeu.qcm;

import colorsHero.jeu.game.CsteJeu;
import colorsHero.jeu.loader.Launcher;
import colorsHero.jeu.score.ScoreManager;
import dvt.jeu.simple.ControleDevint;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;

import static colorsHero.jeu.game.CsteJeu.GAMEDIFFICULTY;
import static colorsHero.jeu.loader.Launcher.LAUNCH_GAME;
import static colorsHero.jeu.loader.Launcher.shootPhase;

public class QCMController extends ControleDevint {

    @FXML
    private Label question;

    @FXML
    private Label result;

    @FXML
    private VBox infoBox;

    @FXML
    private Button answer_1;

    @FXML
    private Button answer_2;

    @FXML
    private Button answer_3;

    @FXML
    private Button answer_4;

    private QCMModel qcmModel;
    private int selectedAnswer;
    private int nbQuestionAsked = 1;

    @Override
    public void init() {
        qcmModel = (QCMModel) model;
        question.setText(qcmModel.question());
        readText(question.getText());
        initAnswer();
        question.setOnMouseClicked(event -> readText(question.getText()));
    }

    @Override
    public void reset() {
        Task<Void> sleeper = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                try {
                    Thread.sleep(2500);
                } catch (InterruptedException e) {
                }
                return null;
            }
        };
        sleeper.setOnSucceeded(event -> {
            if (nbQuestionAsked == 3) {
                shootPhase = true;
                Launcher.launch(LAUNCH_GAME);
                return;
            }
            result.setVisible(false);
            qcmModel.randomize();
            nbQuestionAsked++;
            question.setText(qcmModel.question());
            initAnswer();
            infoBox.setVisible(true);
            readText(question.getText());
        });
        new Thread(sleeper).start();
    }

    @Override
    public void mapTouchToActions() {
        scene.mapKeyPressedToConsumer(KeyCode.F6, x -> readText(question.getText()));
        scene.mapKeyPressedToConsumer(KeyCode.LEFT, x -> left());
        scene.mapKeyPressedToConsumer(KeyCode.RIGHT, x -> right());
        if (GAMEDIFFICULTY == CsteJeu.DIFFICULTY.NORMAL || GAMEDIFFICULTY == CsteJeu.DIFFICULTY.HARD) {
            scene.mapKeyPressedToConsumer(KeyCode.UP, x -> up());
            scene.mapKeyPressedToConsumer(KeyCode.DOWN, x -> down());
        }

    }

    @FXML
    public void validateKey(KeyEvent event) {
        if ((" ".equals(event.getCharacter())) || ("\r".equals(event.getCharacter()))
                || ("\n".equals(event.getCharacter()))) {
            validate();
            reset();
        }
    }

    /**
     * Permet de gérer le déplacement gauche pour sélectionner
     * une réponse.
     */
    public void left() {
        if (selectedAnswer == 2) {
            unSelectButton(answer_2);
            selectedAnswer = 1;
            selectButton(answer_1);
        } else if (selectedAnswer == 4 && GAMEDIFFICULTY == CsteJeu.DIFFICULTY.HARD) {
            unSelectButton(answer_4);
            selectedAnswer = 3;
            selectButton(answer_3);
        }
        readText(selectedAnswerText());
    }

    /**
     * Permet de gérer le déplacement droit pour sélectionner
     * une réponse.
     */
    public void right() {
        if (selectedAnswer == 1) {
            unSelectButton(answer_1);
            selectedAnswer = 2;
            selectButton(answer_2);
        } else if (selectedAnswer == 3 && GAMEDIFFICULTY == CsteJeu.DIFFICULTY.HARD) {
            unSelectButton(answer_3);
            selectedAnswer = 4;
            selectButton(answer_4);
        }
        readText(selectedAnswerText());
    }

    /**
     * Permet de gérer le déplacement haut pour sélectionner
     * une réponse.
     */
    public void up() {
        if (selectedAnswer == 3) {
            unSelectButton(answer_3);
            selectedAnswer = 1;
            selectButton(answer_1);
        } else if (selectedAnswer == 4 && GAMEDIFFICULTY == CsteJeu.DIFFICULTY.HARD) {
            unSelectButton(answer_4);
            selectedAnswer = 2;
            selectButton(answer_2);
        }
        readText(selectedAnswerText());
    }

    /**
     * Permet de gérer le déplacement bas pour sélectionner
     * une réponse.
     */
    public void down() {
        if (selectedAnswer == 1) {
            unSelectButton(answer_1);
            selectedAnswer = 3;
            selectButton(answer_3);
        } else if (selectedAnswer == 2 && GAMEDIFFICULTY == CsteJeu.DIFFICULTY.NORMAL) {
            unSelectButton(answer_2);
            selectedAnswer = 3;
            selectButton(answer_3);
        } else if (selectedAnswer == 2 && GAMEDIFFICULTY == CsteJeu.DIFFICULTY.HARD) {
            unSelectButton(answer_2);
            selectedAnswer = 4;
            selectButton(answer_4);
        }
        readText(selectedAnswerText());
    }

    /**
     * Initialise le bon nombre de réponses possibles selon la difficulté.
     */
    public void initAnswer() {
        selectedAnswer = 1;
        switch (GAMEDIFFICULTY) {
            case NORMAL:
                answer_1.setText(qcmModel.getAnswer(1));
                answer_2.setText(qcmModel.getAnswer(2));
                answer_3.setText(qcmModel.getAnswer(3));
                selectButton(answer_1);
                unSelectButton(answer_2);
                unSelectButton(answer_3);
                break;
            case HARD:
                answer_1.setText(qcmModel.getAnswer(1));
                answer_2.setText(qcmModel.getAnswer(2));
                answer_3.setText(qcmModel.getAnswer(3));
                answer_4.setText(qcmModel.getAnswer(4));
                selectButton(answer_1);
                unSelectButton(answer_2);
                unSelectButton(answer_3);
                unSelectButton(answer_4);
                break;
            default:
                answer_1.setText(qcmModel.getAnswer(1));
                answer_2.setText(qcmModel.getAnswer(2));
                selectButton(answer_1);
                unSelectButton(answer_2);
                break;
        }
    }

    /**
     * Change l'apparence de la réponse actuellement sélectionnée
     *
     * @param selectedButton le réponse sélectionnée
     */
    private void selectButton(Button selectedButton) {
        selectedButton.getStyleClass().clear();
        selectedButton.getStyleClass().add("selectedbutton");
    }

    /**
     * Change l'apparence de la question désélectionnée
     *
     * @param unselectedButton le réponse désélectionnée
     */
    private void unSelectButton(Button unselectedButton) {
        unselectedButton.getStyleClass().clear();
        unselectedButton.getStyleClass().add("unselectedbutton");
    }

    /**
     * Valide la réponse actuellement sélectionnée.
     * Effectue l'affichage de la correction.
     */
    private void validate() {
        infoBox.setVisible(false);
        if (qcmModel.validate(selectedAnswerText())) {
            result.setText("BRAVO ! " + qcmModel.getRightAnswer() + " est la bonne réponse.");
            ScoreManager.score += ((CsteJeu.GAMEDIFFICULTY.getDiff() + 1) * CsteJeu.scorePerAnswer);
            CsteJeu.scoreMultiplier++;
        } else {
            result.setText("Mauvaise réponse. " + selectedAnswerText() + " n'est pas la bonne réponse. "
                    + qcmModel.getRightAnswer() + " est la bonne réponse");
        }
        result.setVisible(true);
    }

    /**
     * Permet d'obtenir le texte de la réponse actuellement sélectionnée.
     *
     * @return le contenu (texte) de la réponse sélectionnée.
     */
    private String selectedAnswerText() {
        switch (selectedAnswer) {
            case 2:
                return answer_2.getText();
            case 3:
                return answer_3.getText();
            case 4:
                return answer_4.getText();
            default:
                return answer_1.getText();
        }
    }

    /**
     * Lit le texte passé en paramètre avec VocalyzeSIVOX.
     *
     * @param textToRead le texte à lire.
     */
    private void readText(String textToRead) {
        scene.getSIVox().stop();
        scene.getSIVox().playText(textToRead);
    }

}
