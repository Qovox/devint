package colorsHero.jeu.qcm.gestionquestions;

import dvt.jeu.simple.ControleDevint;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import static colorsHero.jeu.qcm.gestionquestions.JsonParser.jsonParser;

/**
 * Created by Olivier on 21/05/2017.
 */
public class QCMDelController extends ControleDevint {

    private static final String QCM_MODEL_INIT = "../ressources/questions/questions.json";

    private ObservableList<Question> items;

    private ArrayList<Question> questions;

    private JSONObject listeQ;

    @FXML
    private ListView<Question> liste_questions;

    @FXML
    private Button btn_supprimer;

    @FXML
    private Button btn_cancel;

    @FXML
    private Text test;

    @FXML
    void onCancel(ActionEvent event) {

        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        stage.close();

    }

    @FXML
    void onValidate(ActionEvent event) {

        if (!questions.isEmpty()) {

            listeQ.remove(questions.get(liste_questions.getSelectionModel().getSelectedIndex()).getId());

            questions.remove(liste_questions.getSelectionModel().getSelectedIndex());

            items.remove(liste_questions.getSelectionModel().getSelectedIndex());


            try (FileWriter file = new FileWriter(QCM_MODEL_INIT)) {
                file.write(listeQ.toJSONString());
                file.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    protected void init() {

        listeQ = jsonParser(QCM_MODEL_INIT);

        questions = new ArrayList<>();

        for (Object question : listeQ.keySet()) {

            questions.add(new Question(question.toString(), ((JSONObject) listeQ.get(question)).get("question").toString()));

        }

        items = FXCollections.observableArrayList(questions);
        liste_questions.setItems(items);

    }

    @Override
    protected void reset() {

    }

    @Override
    public void mapTouchToActions() {

    }

}
