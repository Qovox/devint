package colorsHero.jeu.qcm.gestionquestions;

/**
 * Created by Olivier on 21/05/2017.
 */
public class Question {

    private String id;
    private String text;

    public Question(String id, String text) {
        this.id = id;
        this.text = text;
    }

    public String toString() {

        return text;

    }

    public String getId() {
        return id;
    }
}
