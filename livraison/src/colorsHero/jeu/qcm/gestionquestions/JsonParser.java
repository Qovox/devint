package colorsHero.jeu.qcm.gestionquestions;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.BufferedReader;
import java.io.FileReader;

public class JsonParser {

    public static JSONObject jsonParser(String filename) {

        String result;

        JSONParser parser = new JSONParser();

        JSONObject jsonObject = new JSONObject();

        try (FileReader reader = new FileReader(filename)) {

            BufferedReader br = new BufferedReader(reader);
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            while (line != null) {
                sb.append(line);
                line = br.readLine();
            }

            result = sb.toString();

            Object obj = parser.parse(result);

            jsonObject = (JSONObject) obj;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

}