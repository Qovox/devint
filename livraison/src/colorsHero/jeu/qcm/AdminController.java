package colorsHero.jeu.qcm;

import colorsHero.jeu.loader.LoaderDevintSimple;
import dvt.jeu.simple.ControleDevint;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;

/**
 * Created by Olivier on 21/05/2017.
 */
public class AdminController extends ControleDevint {

    @FXML
    private Button addBtn;

    @FXML
    private Button delBtn;

    @FXML
    void Add(MouseEvent event) {

        new LoaderDevintSimple("Panneau Administrateur", "questions/questionsAdd.fxml", new VBox(), null);

    }

    @FXML
    void Del(MouseEvent event) {

        new LoaderDevintSimple("Panneau Administrateur", "questions/questionsDel.fxml", new VBox(), null);

    }

    @Override
    protected void init() {

    }

    @Override
    protected void reset() {

    }

    @Override
    public void mapTouchToActions() {

    }
}
