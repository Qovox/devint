package colorsHero.jeu.score;

/**
 * Created by Enzo on 23/05/2017.
 */
public class BestScore {

    private final int score;
    private final String name;

    public BestScore(String name, int score) {
        this.name = name;
        this.score = score;
    }

    public int getScore() {
        return score;
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return name + " - Score : " + score;
    }

}
