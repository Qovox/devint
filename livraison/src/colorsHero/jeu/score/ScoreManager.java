package colorsHero.jeu.score;

import java.util.List;

/**
 * Created by Enzo on 23/05/2017.
 */
public class ScoreManager {

    public static int score = 0;
    private final String fileScore = "../ressources/scores/HSColorsHero.json";

    private List<BestScore> bestScoreList;

    public ScoreManager() {
        bestScoreList = JSONScoreParser.extractBestScores(fileScore);
    }

    public boolean isNewBestScore() {
        for (BestScore best : bestScoreList) {
            if (score > best.getScore()) {
                return true;
            }
        }
        return false;
    }

    public void rewriteBestScores(String name) {

        bestScoreList.add(new BestScore(name, score));

        bestScoreList.sort((BestScore b1, BestScore b2) -> {
            if (b1.getScore() > b2.getScore())
                return -1;
            if (b1.getScore() < b2.getScore())
                return 1;
            return 0;
        });

        bestScoreList.remove(bestScoreList.size() - 1);

        JSONScoreParser.rewriteBestScores(fileScore, bestScoreList);
    }

    public List<BestScore> getBestScoreList() {
        return bestScoreList;
    }

}
