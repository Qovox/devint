package colorsHero.jeu.score;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Enzo on 23/05/2017.
 */
public class JSONScoreParser {

    private static JSONArray jsonScoreParser(String filename) {
        FileReader fileReader = null;
        JSONParser jsonParser = new JSONParser();
        JSONArray jsonArray = new JSONArray();

        try {
            fileReader = new FileReader(filename);
            jsonArray = (JSONArray) jsonParser.parse(fileReader);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return jsonArray;
    }

    public static List<BestScore> extractBestScores(String filename) {
        List<BestScore> res = new ArrayList<>();

        JSONArray tmp = jsonScoreParser(filename);

        for (int i = 0; i < tmp.size(); i++) {
            JSONObject tmpScore = (JSONObject) ((JSONObject) tmp.get(i)).get("score");

            res.add(new BestScore((String) tmpScore.get("nom"), Integer.valueOf((String) tmpScore.get("score"))));
        }

        return res;
    }

    public static void rewriteBestScores(String fileScore, List<BestScore> bestScoreList) {

        JSONArray tmp = new JSONArray();

        for (int i = 0; i < bestScoreList.size(); i++) {
            tmp.add(i, parseToJSON(bestScoreList.get(i)));
        }

        try (FileWriter file = new FileWriter(fileScore)) {
            file.write(tmp.toJSONString());
            file.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private static JSONObject parseToJSON(BestScore bestScore) {
        JSONObject res = new JSONObject();

        JSONObject tmp = new JSONObject();

        tmp.put("nom", bestScore.getName());
        tmp.put("score", String.valueOf(bestScore.getScore()));

        res.put("score", tmp);

        return res;
    }

}
