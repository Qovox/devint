package colorsHero.tools;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class QCMManager {

    public static List<String> questionsToAsk;

    static {
        questionsToAsk = new ArrayList<>();
    }

    public QCMManager() {

    }

    public static void checkQuestions() {
        if (questionsToAsk.size() < 3)
            QCMManager.setQuestionsToAsk();
    }

    public static void setQuestionsToAsk() {
        questionsToAsk = new ArrayList<>();
        JSONObject data = null;
        try {
            data = (JSONObject) new JSONParser().parse(new FileReader("../ressources/questions/questions.json"));
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        for (Object question : data.keySet()) {
            questionsToAsk.add((String) question);
        }
    }

}
