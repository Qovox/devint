package dvt.jeu.simple;


import dvt.devint.ConstantesDevint;
import dvt.devint.SceneDevint;
import javafx.scene.Parent;
import javafx.scene.input.KeyCode;
import javafx.stage.Stage;

/**
 * classe à étendre pour créer un jeu Devint avec une IHM immobile qui se rafraîchit
 * selon les évênements utilisateur (par exemple, pour faire un quizz)
 *
 * @author helen
 */
public abstract class JeuDevint extends Stage {

    /**
     * le controle du jeu
     */
    protected ControleDevint control;

    /**
     * le modele du jeu
     */
    protected ModeleDevint model;

    /**
     * l'éventuel fichier fxml de la scene
     */
    protected String fxml_file;

    /**
     * le titre de la scene
     */
    protected String title;

    /**
     * le type d'élément graphique de la scene
     */
    protected Parent parent;

    /**
     * constructeur du jeu
     */
    public JeuDevint() {
        initialize();
    }

    public JeuDevint(String title, String fxml_file, Parent parent, ModeleDevint model) {
        this.title = title;
        this.fxml_file = ConstantesDevint.ressourcesFileName(fxml_file);
        this.parent = parent;
        this.model = model;
        initialize();
    }

    private void initialize() {
        this.model = initModel();
        this.control = initControlAndScene();
        this.control.setModel(model);
        this.control.mapTouchToActions();
        this.control.init();
        this.setScene(control.getScene());
        this.setTitle(titre());
        sceneKeyBind();
        this.show();
    }

    private void sceneKeyBind() {
        setFullScreenExitHint("");
        setFullScreen(true);
        ((SceneDevint) this.getScene()).mapKeyPressedToConsumer(KeyCode.F11, x -> setFullScreen(true));
        ((SceneDevint) this.getScene()).mapKeyPressedToConsumer(KeyCode.F12, x -> setFullScreen(false));
    }

    /**
     * le titre du jeu
     *
     * @return le titre
     */
    public abstract String titre();

    /**
     * initialisation du modèle
     * méthode appelée dans le constructeur
     */
    protected abstract ModeleDevint initModel();

    /**
     * initialisation du contrôle devint et de la scene devint
     * méthode appelée dans le constructeur
     * la scene peut être construite à partir du FXML via un FXMLLoader()
     * <p>
     * on peut aussi tout créer à la main si on ne veut pas passer par FXML
     */
    protected abstract ControleDevint initControlAndScene();

}
