package dvt.jeu.animation;

import dvt.devint.ConstantesDevint;
import dvt.devint.SceneDevint;
import javafx.animation.AnimationTimer;
import javafx.scene.Parent;
import javafx.scene.input.KeyCode;
import javafx.stage.Stage;

/**
 * classe à étendre pour créer un jeu Devint avec une boucle de jeu.
 * A chaque étape de la boucle, le modèle est mis à jour et la vue est rafraichie
 *
 * @author helen
 */
public abstract class JeuAnimationDevint extends Stage {

    /**
     * l'éventuel fichier fxml de la scene
     */
    protected String fxml_file;

    /**
     * le titre de la scene
     */
    protected String title;

    /**
     * le type d'élément graphique de la scene
     */
    protected Parent parent;

    /**
     * le controle du jeu
     */
    protected ControleAnimationDevint control;

    /**
     * le modele du jeu
     */
    protected ModeleAnimationDevint model;
    private AnimationTimer animTimer;

    /**
     * constructeur du jeu
     * initialise le controle et le modèle et lance la boucle de jeu
     */
    public JeuAnimationDevint() {
        initialize();
    }

    public JeuAnimationDevint(String title, String fxml_file, Parent parent, ModeleAnimationDevint model) {
        this.title = title;
        this.fxml_file = ConstantesDevint.ressourcesFileName(fxml_file);
        this.parent = parent;
        this.model = model;
        initialize();
    }

    /**
     * initialise le controle et le modèle et lance la boucle de jeu
     */
    private void initialize() {
        // créé le modèle
        this.model = initModel();
        // créé le contrôle et la scene
        // quand on passe par FXML on récupère le contrôle et la
        // scene via le FXMLloader
        this.control = initControlAndScene();
        // ajoute le lien touches/actions
        this.control.mapTouchToActions();
        // le contrôle a accès au modèle
        this.control.setModel(model);
        // initialise le controle => affichage de la vue de départ

        //TODO ATTENTION, ICI LE INIT SE FAIT 2x ET CELA PEUT FAIRE PLANTER LE PROGRAMME. BUG DU SYSTEME DEVINT ?
        //this.control.init();

        this.setScene(control.getScene());
        this.setTitle(titre());
        sceneKeyBind();
        this.show();
        // lance la boucle de jeu : update du modèle et render du controle
        // à chaque tour.
        this.loop();
    }

    private void sceneKeyBind() {
        setFullScreenExitHint("");
        setFullScreen(true);
        ((SceneDevint) this.getScene()).mapKeyPressedToConsumer(KeyCode.F11, x -> setFullScreen(true));
        ((SceneDevint) this.getScene()).mapKeyPressedToConsumer(KeyCode.F12, x -> setFullScreen(false));
    }

    /**
     * La boucle du jeu qui permet de garder un FPS (frame per seconds) constant
     * peu importe le PC
     */
    private void loop() {

        this.animTimer = new AnimationTimer() {
            @Override
            public void handle(long currentNanoTime) {
                model.update();
                control.render();
            }

        };

        // initialisation des valeurs
        control.init(animTimer);

        animTimer.start();

    }

    /**
     * le titre du jeu
     *
     * @return le titre
     */
    public abstract String titre();

    /**
     * initialisation du modèle
     * méthode appelée dans le constructeur
     */
    protected abstract ModeleAnimationDevint initModel();

    /**
     * initialisation du contrôle devint et de la scene devint
     * méthode appelée dans le constructeur
     * la scene peut être construite à partir du FXML via un FXMLLoader()
     * <p>
     * on peut aussi tout créer à la main si on ne veut pas passer par FXML
     */
    protected abstract ControleAnimationDevint initControlAndScene();

}
