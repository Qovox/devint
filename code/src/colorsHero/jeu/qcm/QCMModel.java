package colorsHero.jeu.qcm;

import colorsHero.jeu.game.CsteJeu;
import colorsHero.tools.QCMManager;
import dvt.jeu.simple.ModeleDevint;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.util.Random;

import static colorsHero.tools.QCMManager.questionsToAsk;

public class QCMModel extends ModeleDevint {

    private JSONObject data;
    private JSONObject question;
    private int random;
    private boolean putCorrectAnswer = false;

    /**
     * Constructeur du modèle pour le jeu du QCM.
     *
     * @param questionPath le chemin pour accéder à la base de données des questions.
     * @throws IOException    déclenchée si le chemin questionPath n'est pas correcte.
     * @throws ParseException déclenchée si le fichier spécifié par questionPath est syntaxiquement incorrecte.
     */
    public QCMModel(String questionPath) throws IOException, ParseException {
        QCMManager.checkQuestions();
        data = (JSONObject) new JSONParser().parse(new FileReader(questionPath));
        randomize();
    }

    /**
     * Obtient la question à poser (choisie aléatoirement).
     *
     * @return la question à poser.
     */
    public String question() {
        return (String) question.get("question");
    }

    void randomize() {
        putCorrectAnswer = false;
        Random rdm = new Random();
        random = rdm.nextInt(questionsToAsk.size() - 1);
        question = (JSONObject) data.get(questionsToAsk.get(random));

        questionsToAsk.remove(random);
    }

    /**
     * Vérifie et valide ou non la réponse sélectionnée.
     *
     * @param selectedAnswer la réponse actuellement sélectionnée.
     * @return true si selectedAnswer est la bonne réponse, false sinon.
     */
    boolean validate(String selectedAnswer) {
        return getRightAnswer().equals(selectedAnswer);
    }

    /**
     * Obtient la bonne réponse à la question posée.
     *
     * @return la bonne réponse à la question posée.
     */
    String getRightAnswer() {
        return (String) question.get("right_answer");
    }

    /**
     * Obtient une réponse selon un indice de proposition de réponse.
     *
     * @param i l'indice de la proposition de réponse.
     * @return la réponse selon l'indice passé en paramètre.
     */
    String getAnswer(int i) {
        Random rdm = new Random();
        String result;
        if (!putCorrectAnswer && rdm.nextBoolean()) {
            putCorrectAnswer = true;
            result = getRightAnswer();
        } else if (putCorrectAnswer) {
            result = (String) question.get("answer_" + (i - 1));
        } else result = (String) question.get("answer_" + i);
        result = checkCorrectAnswer(result, i);
        return result;
    }

    /**
     * Vérifie si la bonne réponse est bien présente avant d'afficher la dernière
     * proposition possible.
     *
     * @param currentAnswer la prochaine réponse qui est censée s'afficher.
     * @param i             l'indice actuel.
     * @return la bonne réponse à afficher.
     */
    private String checkCorrectAnswer(String currentAnswer, int i) {
        switch (CsteJeu.getGameDifficulty()) {
            case EASY:
                if (i == 2 && !putCorrectAnswer)
                    return getRightAnswer();
                break;
            case NORMAL:
                if (i == 3 && !putCorrectAnswer)
                    return getRightAnswer();
                break;
            case HARD:
                if (i == 4 && !putCorrectAnswer)
                    return getRightAnswer();
                break;
            default:
                return currentAnswer;
        }
        return currentAnswer;
    }

}
