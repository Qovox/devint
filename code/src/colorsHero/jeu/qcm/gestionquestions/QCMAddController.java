package colorsHero.jeu.qcm.gestionquestions;

import dvt.jeu.simple.ControleDevint;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.io.IOException;

import static colorsHero.jeu.qcm.gestionquestions.JsonParser.jsonParser;
import static colorsHero.tools.TextHelper.*;

public class QCMAddController extends ControleDevint {

    private static final String QCM_MODEL_INIT = "../ressources/questions/questions.json";

    @FXML
    private TextField question_field;

    @FXML
    private TextField txt_rep1;

    @FXML
    private TextField txt_rep2;

    @FXML
    private TextField txt_rep3;

    @FXML
    private TextField txt_rep4;

    @FXML
    private Button btn_validate;

    @FXML
    private Button btn_cancel;

    @FXML
    private Text txt_info;

    @FXML
    void onCancel(ActionEvent event) {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        stage.close();
    }

    @FXML
    void onValidate(ActionEvent event) {

        TextField[] txtFields = new TextField[5];

        txtFields[0] = question_field;
        txtFields[1] = txt_rep1;
        txtFields[2] = txt_rep2;
        txtFields[3] = txt_rep3;
        txtFields[4] = txt_rep4;


        for (TextField txtField : txtFields) {

            if ("".equals(txtField.getText())) {

                txtField.requestFocus();
                txt_info.setText("Remplir le champs manquant");
                txt_info.setVisible(true);
                return;

            }

        }

        JSONObject jsonObject = jsonParser(QCM_MODEL_INIT);

        int cpt = 0;

        while (jsonObject.containsKey("question_" + cpt)) {

            cpt++;

        }

        String id = "question_" + cpt;
        String question = question_field.getText();
        String right_answer = txt_rep1.getText();
        String answer_1 = txt_rep2.getText();
        String answer_2 = txt_rep3.getText();
        String answer_3 = txt_rep4.getText();

        JSONObject newQuestion = new JSONObject();

        newQuestion.put("question", question);
        newQuestion.put("answer_1", answer_1);
        newQuestion.put("answer_2", answer_2);
        newQuestion.put("answer_3", answer_3);
        newQuestion.put("right_answer", right_answer);

        jsonObject.put(id, newQuestion);

        //Parse to JSON

        try (FileWriter file = new FileWriter(QCM_MODEL_INIT)) {
            file.write(jsonObject.toJSONString());
            file.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }


        question_field.clear();

        txt_rep1.clear();
        txt_rep2.clear();
        txt_rep3.clear();
        txt_rep4.clear();

        txt_info.setText("Question ajoutée");
        txt_info.setVisible(true);

    }

    /**
     * methode appelee avant la boucle de jeu
     * utilisee pour l'itialisation du modèle et de la vue
     */
    @Override
    protected void init() {

    }

    /**
     * methode appelee quand on active la touche pour re-commencer le jeu
     * NOTA : si votre jeu se joue à l'infini, redéfinir cette fonction
     * en ne faisant rien
     */
    @Override
    protected void reset() {

    }

    /**
     * méthode appelée pour associer les actions aux touches dans la scene devint
     * est un ensemble d'instructions de la forme
     * scene.mapKeyPressedToConsumer(KeyCode.<touche>, (x) -> <fonction>);
     * où <touche> est une touche du clavier et <fonction> est un appel de fonction du contrôle
     */
    @Override
    public void mapTouchToActions() {
        scene.mapKeyPressedToConsumer(KeyCode.BACK_SPACE, x -> backSpace(scene.focusOwnerProperty().get()));
        scene.mapKeyPressedToConsumer(KeyCode.LEFT, x -> caretLeft(scene.focusOwnerProperty().get()));
        scene.mapKeyPressedToConsumer(KeyCode.RIGHT, x -> caretRight(scene.focusOwnerProperty().get()));
    }

}
