package colorsHero.jeu.game.entity;

import colorsHero.jeu.game.CsteJeu;

import java.awt.*;

public class Monster extends Entity {

    /**
     * Monster constructor
     *
     * @param weapon   the initial weapon of the monster
     * @param position the initial position of the monster
     */
    public Monster(Weapon weapon, Point position) {
        super(weapon, position);

        natureType = NatureType.getRandom(CsteJeu.getGameDifficulty());
        this.life = CsteJeu.MONSTERLIFE;
    }

    public boolean isOutOfScreen() {
        return this.topPosition.getX() <= 0 && this.topPosition.y >= 0;
    }

    public boolean isInstantiated() {
        return this.topPosition.y < 0;
    }

}
