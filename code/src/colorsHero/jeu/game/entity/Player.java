package colorsHero.jeu.game.entity;

import colorsHero.jeu.game.CsteJeu;
import colorsHero.jeu.game.tools.SpellSelector;

import java.awt.*;

/**
 * Created by Alexandre on 29/03/2017.
 */
public class Player extends Entity {

    private SpellSelector spellSelector;
    private boolean canFire;

    protected long fireTimer;
    protected long fireCounter;

    /**
     * Player constructor
     *
     * @param weapon   the initial weapon of the player
     * @param position the initial position of the player
     */
    public Player(Weapon weapon, Point position) {
        super(weapon, position);

        this.natureType = weapon.getNatureType();
        this.spellSelector = new SpellSelector();
        this.canFire = true;

        fireTimer = 0;
        fireCounter = System.currentTimeMillis();

        reset();
    }

    public void updateTimer() {
        long deltaTime = System.currentTimeMillis() - fireCounter;
        fireTimer += deltaTime;
    }

    public boolean isGameOver() {
        return this.life <= 0;
    }

    public void reset() {
        this.life = CsteJeu.PLAYERLIFE;
    }

    public void switchPrevSpell(String imagePath) {
        NatureType tmp = spellSelector.getPreviousNature();

        this.weapon.setNatureType(tmp);
        this.natureType = tmp;

        this.initEntitySprite(imagePath);
    }

    public void switchNextSpell(String imagePath) {
        NatureType tmp = spellSelector.getNextNature();

        this.weapon.setNatureType(tmp);
        this.natureType = tmp;

        this.initEntitySprite(imagePath);
    }

    /**
     * Method that create a spell, depending on the entity's current nature type
     *
     * @return a spell with the current entity's nature type
     */
    public Spell fireSpell() {
        int middleYPos = topPosition.y + ((int) image.getImage().getHeight() / 2) - CsteJeu.HEIGHTSPELL / 2;
        Point position = new Point((int) image.getImage().getWidth(), middleYPos);

        return new Spell(position, getWeaponNatureType());
    }

    public void resetTimer() {
        this.fireCounter = System.currentTimeMillis();
        this.fireTimer = 0;
    }

    public boolean canFire() {
        return fireTimer > CsteJeu.playerFireTimer;
    }

}
