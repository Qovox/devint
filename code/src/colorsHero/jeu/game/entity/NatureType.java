package colorsHero.jeu.game.entity;

import colorsHero.jeu.game.CsteJeu;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.List;

public enum NatureType {

    FIRE(Color.RED, CsteJeu.DIFFICULTY.EASY),
    WATER(Color.web("0x0000B8"), CsteJeu.DIFFICULTY.EASY),
    NATURE(Color.web("0x00C100"), CsteJeu.DIFFICULTY.NORMAL),
    THUNDER(Color.web("0xF1D700"), CsteJeu.DIFFICULTY.HARD);

    private Color color;
    private CsteJeu.DIFFICULTY difficulty;

    /**
     * NatureType constructor
     *
     * @param color the color of the type
     */
    NatureType(Color color, CsteJeu.DIFFICULTY difficulty) {
        this.color = color;
        this.difficulty = difficulty;
    }

    /**
     * Nature type color getter
     *
     * @return the color of the nature type
     */
    public Color getColor() {
        return color;
    }

    public static NatureType getRandom(CsteJeu.DIFFICULTY difficulty) {
        List<NatureType> tmp = getWithDifficulty(difficulty);

        return tmp.get((int) (Math.random() * tmp.size()));
    }

    public static List<NatureType> getWithDifficulty(CsteJeu.DIFFICULTY difficulty) {
        int diffWanted = difficulty.getDiff();
        List<NatureType> res = new ArrayList<>();

        for (NatureType nature : values()) {
            if (nature.difficulty.getDiff() <= diffWanted) {
                res.add(nature);
            }
        }
        return res;
    }

}
