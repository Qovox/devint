package colorsHero.jeu.game.entity;

import colorsHero.jeu.game.CsteJeu;
import colorsHero.jeu.game.GameController;

import java.awt.*;

/**
 * Created by Enzo on 17/05/2017.
 */
public class Boss extends Entity {

    private boolean goDown;

    /**
     * Entity constructor
     *
     * @param weapon   the initial weapon of the entity
     * @param position the initial position of the entity
     */
    public Boss(Weapon weapon, Point position) {
        super(weapon, position);

        natureType = NatureType.getRandom(CsteJeu.GAMEDIFFICULTY);
        this.life = CsteJeu.BOSSLIFE;
    }

    public void changeColor(String s) {
        natureType = NatureType.getRandom(CsteJeu.GAMEDIFFICULTY);

        this.initEntitySprite(s);
    }

    public void tookDamage(NatureType natureType) {
        if (natureType == this.natureType) {
            this.life--;
        }
    }

    public void checkMovingState(Double height) {
        if (goDown) {
            checkDown(height);
        } else {
            checkUp();
        }
    }

    private void checkUp() {
        if (this.getTopPosition().y <= 0) {
            goDown = true;
        }
    }

    private void checkDown(Double height) {
        if (this.isUnderGround(height - GameController.LAYOUT_SIZE)) {
            goDown = false;
        }
    }

    public void moveBoss() {
        if (goDown) {
            this.topPosition.y += CsteJeu.BOSSSPEED;
        } else {
            this.topPosition.y -= CsteJeu.BOSSSPEED;
        }
    }

}
