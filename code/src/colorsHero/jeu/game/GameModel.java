package colorsHero.jeu.game;

import colorsHero.jeu.game.entity.NatureType;
import colorsHero.jeu.game.entity.Player;
import colorsHero.jeu.game.entity.Weapon;
import colorsHero.jeu.game.managers.BossManager;
import colorsHero.jeu.game.managers.EnemyManager;
import colorsHero.jeu.game.managers.MonsterManager;
import colorsHero.jeu.game.managers.SpellManager;
import colorsHero.jeu.game.tools.HitboxTool;
import colorsHero.jeu.loader.Launcher;
import dvt.jeu.animation.ModeleAnimationDevint;

import java.awt.*;

public class GameModel extends ModeleAnimationDevint {

    private Player player;
    private MonsterManager monsterManager;
    private SpellManager spellManager;
    private HitboxTool hitboxTool;

    public GameModel() {
        this.player = new Player(new Weapon(1, NatureType.FIRE), new Point(0, 150));
        this.spellManager = new SpellManager();
        this.hitboxTool = new HitboxTool();

        player.initEntitySprite(this.pathImagePlayer());
        initBossPhase();
    }

    private void initBossPhase() {
        if (Launcher.wave == Launcher.WAVE_NB) {
            CsteJeu.isBoss = true;
            monsterManager = new BossManager(pathImageBoss());
        } else {
            CsteJeu.isBoss = false;
            monsterManager = new EnemyManager();
        }
    }

    public String pathMinionBoss() {
        return "images/monster.png";
    }

    public String pathImageBoss() {
        return "images/boss3.png";
    }

    public String pathImagePlayer() {
        return "images/mage.png";
    }

    public String pathImageEnemy() {
        return "images/monster.png";
    }

    @Override
    protected void update() {
        if (GameController.playerWon) {

        } else if (player.isGameOver()) {

        } else {
            this.spellManager.updatePlayerSpells();

            if (CsteJeu.isBoss) {
                ((BossManager) this.monsterManager).spawnMonsters(pathImageBoss(), pathMinionBoss());
                this.hitboxTool.checkBossAndSpells(spellManager.getSpells(), ((BossManager) monsterManager).getBoss());
                this.hitboxTool.checkMonsterAndSpells(spellManager.getSpells(), ((BossManager) monsterManager).getBossMinionManager().getMonsterList());
            } else {
                ((EnemyManager) this.monsterManager).spawnMonsters(pathImageEnemy());
                this.monsterManager.updateMonstersPositions(player, 0);
                this.hitboxTool.checkMonsterAndSpells(spellManager.getSpells(), ((EnemyManager) monsterManager).getMonsterList());
            }
        }
    }

    public Player getPlayer() {
        return player;
    }

    public SpellManager getSpellManager() {
        return spellManager;
    }

    public MonsterManager getMonsterManager() {
        return monsterManager;
    }
}
