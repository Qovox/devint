package colorsHero.jeu.game.tools;

import colorsHero.jeu.game.entity.Boss;
import colorsHero.jeu.game.entity.Monster;
import colorsHero.jeu.game.entity.Spell;
import javafx.geometry.Bounds;

import java.util.List;

/**
 * Created by Enzo on 02/04/2017.
 */
public class HitboxTool {

    public HitboxTool() {

    }

    public void checkMonsterAndSpells(List<Spell> spellList, List<Monster> monsterList) {
        for (Monster monster : monsterList) {

            for (Spell spell : spellList) {
                Bounds boundsSpell = spell.getRectangle().localToScene(spell.getRectangle().getBoundsInLocal());

                if (monster.isHit(boundsSpell)) {
                    monster.tookDamage(spell.getNatureType());
                    spell.setMustBeRemoved(true);
                }
            }
        }
    }

    public void checkBossAndSpells(List<Spell> spells, Boss boss) {
        for (Spell spell : spells) {
            Bounds boundsSpell = spell.getRectangle().localToScene(spell.getRectangle().getBoundsInLocal());

            if (boss.isHit(boundsSpell)) {
                boss.tookDamage(spell.getNatureType());
                spell.setMustBeRemoved(true);
            }
        }
    }

}
