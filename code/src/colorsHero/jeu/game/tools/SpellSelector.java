package colorsHero.jeu.game.tools;

import colorsHero.jeu.game.CsteJeu;
import colorsHero.jeu.game.entity.NatureType;

import java.util.List;

/**
 * Created by Enzo on 26/04/2017.
 */
public class SpellSelector {

    private List<NatureType> natureTypeList;
    private int index;

    public SpellSelector() {
        natureTypeList = NatureType.getWithDifficulty(CsteJeu.getGameDifficulty());
    }

    public NatureType getNextNature() {
        index++;

        if (index > (natureTypeList.size() - 1)) {
            index = 0;
        }

        return natureTypeList.get(index);
    }

    public NatureType getPreviousNature() {
        index--;

        if (index < 0) {
            index = natureTypeList.size() - 1;
        }

        return natureTypeList.get(index);
    }

}
