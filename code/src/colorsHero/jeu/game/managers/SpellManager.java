package colorsHero.jeu.game.managers;

import colorsHero.jeu.game.entity.Spell;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Enzo on 02/04/2017.
 */
public class SpellManager {

    private List<Spell> spells;

    public SpellManager() {
        spells = new ArrayList<>();
    }

    public void addSpell(Spell spell, BoardManager boardManager) {
        spell.initRectangle();

        boardManager.addSpellToCanvas(spell);

        spells.add(spell);
    }

    public void flushSpellsFromGame(BoardManager boardManager) {
        boardManager.clearAllSpells(spells);
        spells = new ArrayList<>();
    }

    /**
     * Méthode permettant de mettre a jour la position des sorts du joueur
     */
    public void updatePlayerSpells() {
        for (Spell spell : spells) {
            spell.moveStraight();
        }
    }

    public void visualSpellUpdate(BoardManager boardManager) {
        Iterator<Spell> spellIterator = spells.iterator();

        while (spellIterator.hasNext()) {
            Spell spell = spellIterator.next();

            spell.visualMoveStraight();

            if (spell.isOutOfScreen(boardManager.getWidthScreen()) || spell.getMustBeRemoved()) {
                boardManager.removeSpellFromCanvas(spell);
                spellIterator.remove();
            }
        }
    }

    public List<Spell> getSpells() {
        return spells;
    }

}
