package colorsHero.jeu.game.managers;

import colorsHero.jeu.game.CsteJeu;
import colorsHero.jeu.game.entity.Boss;
import colorsHero.jeu.game.entity.NatureType;
import colorsHero.jeu.game.entity.Player;
import colorsHero.jeu.game.entity.Weapon;
import colorsHero.jeu.score.ScoreManager;

import java.awt.*;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Enzo on 17/05/2017.
 */
public class BossManager extends MonsterManager {

    private Boss boss;
    private BossMinionManager bossMinionManager;

    private boolean isInstantiated = false;

    private long timerColor = 0;
    private long spawnTimeCountColor;

    public BossManager(String s) {
        boss = new Boss(new Weapon(1, NatureType.THUNDER), new Point(0, -1));
        boss.initEntitySprite(s);
        bossMinionManager = new BossMinionManager();

        resetTimer();
        resetTimerColor();
    }

    public void spawnMonsters(String s, String pathImageEnemy) {
        switchColorBoss(s);

        if (bossMinionManager.spawnMonsters(pathImageEnemy)) {
            notifyOthers();
        }
    }

    private void switchColorBoss(String s) {
        long deltaTime = System.currentTimeMillis() - spawnTimeCountColor;
        timerColor += deltaTime;

        if (timerColor > CsteJeu.BOSSCOLORSWITCH) {
            resetTimerColor();

            boss.changeColor(s);
        }
    }

    @Override
    public void updateMonstersPositions(Player player, double height) {
        boss.checkMovingState(height);

        boss.moveBoss();

        bossMinionManager.updateMonstersPositions(player, height);
    }

    @Override
    public void flushMonstersFromGame(BoardManager boardManager) {
        boardManager.removeEntityFromCanvas(boss);
        bossMinionManager.flushMonstersFromGame(boardManager);
    }

    @Override
    public void deleteMonstersFromGame(BoardManager boardManager) {
        if (boss.isDead()) {
            flushMonstersFromGame(boardManager);
            ScoreManager.score += ((CsteJeu.GAMEDIFFICULTY.getDiff() + 1) * CsteJeu.scoreMultiplier * CsteJeu.scoreBoss);
            notifyOthers();
        }

        if (bossMinionManager.deleteMinionsFromGame(boardManager)) {
            notifyOthers();
        }
    }

    @Override
    public void visualMonstersUpdate(BoardManager boardManager, Player player) {
        if (!isInstantiated) {
            int playerHeight = (int) (player.getImage().getImage().getHeight() / 2);
            int randPos = ThreadLocalRandom.current().nextInt(playerHeight, (int) boardManager.getHeightScreen() - playerHeight + 1);

            boss.setLocation(new Point((int) boardManager.getWidthScreen() - (int) boss.getRectangle().getWidth(), randPos));

            boardManager.addEntityToCanvas(boss);

            isInstantiated = true;
        }

        boss.refreshSpritePosition();

        bossMinionManager.visualMonstersUpdate(boardManager, player);
    }

    public void notifyOthers() {
        setChanged();
        if (boss.isDead()) {
            notifyObservers(0);
        } else {
            notifyObservers(1 + bossMinionManager.getMonsterList().size());
        }
    }

    @Override
    public void reset(BoardManager boardManager) {
        this.flushMonstersFromGame(boardManager);

        resetTimer();
    }

    private void resetTimerColor() {
        this.timerColor = 0;
        this.spawnTimeCountColor = System.currentTimeMillis();
    }

    public Boss getBoss() {
        return boss;
    }

    public BossMinionManager getBossMinionManager() {
        return bossMinionManager;
    }

}
