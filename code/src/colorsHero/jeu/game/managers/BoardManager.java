package colorsHero.jeu.game.managers;

import colorsHero.jeu.game.GameController;
import colorsHero.jeu.game.entity.Entity;
import colorsHero.jeu.game.entity.Monster;
import colorsHero.jeu.game.entity.Spell;
import dvt.devint.SceneDevint;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

import java.util.List;


/**
 * Created by Enzo on 01/04/2017.
 */
public class BoardManager {

    private SceneDevint scene;

    /**
     * BoardManager constructor
     *
     * @param scene the current game's scene
     */
    public BoardManager(Scene scene) {
        this.scene = (SceneDevint) scene;
    }

    /**
     * Méthode that remove the sprite part of a spell
     *
     * @param spell the spell we want to remove
     */
    public void removeSpellFromCanvas(Spell spell) {
        VBox group = (VBox) scene.getRoot();
        AnchorPane anchorPane = (AnchorPane) group.getChildren().get(1);

        anchorPane.getChildren().remove(spell.getRectangle());
    }

    /**
     * Méthod that remove the sprite part of an entity
     *
     * @param entity the entity we want to remove
     */
    public void removeEntityFromCanvas(Entity entity) {
        VBox group = (VBox) scene.getRoot();
        AnchorPane anchorPane = (AnchorPane) group.getChildren().get(1);

        anchorPane.getChildren().remove(entity.getImage());
        anchorPane.getChildren().remove(entity.getRectangle());
    }

    /**
     * Method that add a spell sprite to the game
     *
     * @param spell the spell we want to add
     */
    public void addSpellToCanvas(Spell spell) {
        VBox group = (VBox) scene.getRoot();
        AnchorPane anchorPane = (AnchorPane) group.getChildren().get(1);

        anchorPane.getChildren().add(spell.getRectangle());
    }

    /**
     * Method that add an entity sprite to the game
     *
     * @param entity the entity we want to add
     */
    public void addEntityToCanvas(Entity entity) {
        VBox group = (VBox) scene.getRoot();
        AnchorPane anchorPane = (AnchorPane) group.getChildren().get(1);

        anchorPane.getChildren().add(entity.getImage());
        anchorPane.getChildren().add(entity.getRectangle());
    }

    /**
     * Scene width getter
     *
     * @return the width of the screen
     */
    public double getWidthScreen() {
        return this.scene.getWidth();
    }

    /**
     * Scene height getter
     *
     * @return the height of the screen
     */
    public double getHeightScreen() {
        return this.scene.getHeight() - GameController.LAYOUT_SIZE;
    }

    public void clearAllSpells(List<Spell> spells) {
        for (Spell spell :
                spells) {
            removeSpellFromCanvas(spell);
        }
    }

    public void clearAllMonsters(List<Monster> monsters) {
        for (Entity entity :
                monsters) {
            removeEntityFromCanvas(entity);
        }
    }

}
