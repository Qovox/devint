package colorsHero.jeu.game;

import colorsHero.jeu.game.CsteJeu.DIFFICULTY;
import colorsHero.jeu.game.entity.Player;
import colorsHero.jeu.game.managers.BoardManager;
import colorsHero.jeu.game.managers.MonsterManager;
import colorsHero.jeu.game.managers.SpellManager;
import colorsHero.jeu.loader.Launcher;
import colorsHero.jeu.score.ScoreManager;
import dvt.devint.ConstantesDevint;
import dvt.jeu.animation.ControleAnimationDevint;
import javafx.animation.AnimationTimer;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;

import java.util.Observable;
import java.util.Observer;

import static colorsHero.jeu.game.CsteJeu.GAMEDIFFICULTY;
import static colorsHero.jeu.game.CsteJeu.isBoss;

public class GameController extends ControleAnimationDevint implements Observer {

    @FXML
    private ImageView heart1;

    @FXML
    private ImageView heart2;

    @FXML
    private ImageView heart3;

    @FXML
    private Label scoreLabel;

    private BoardManager boardManager;
    private Player player;

    private AnimationTimer animationTimer;

    private boolean up, down, left, right;
    private int selectedSpell;
    private SpellManager spellManager;
    private MonsterManager monsterManager;

    private static final Image EMPTY_HEART = new Image(ConstantesDevint.ressourcesFileName("images/empty_heart.png"));
    private static final Image HALF_HEART = new Image(ConstantesDevint.ressourcesFileName("images/half_heart.png"));
    private static final Image FULL_HEART = new Image(ConstantesDevint.ressourcesFileName("images/full_heart.png"));

    public static final double LAYOUT_SIZE = 150;
    public static boolean playerWon;

    /**
     * methode appelee avant la boucle de jeu
     * utilisee pour l'initialisation du modèle et de la vue
     *
     * @param animTimer
     */
    @Override
    public void init(AnimationTimer animTimer) {
        GameModel modeleGame = (GameModel) model;
        this.player = modeleGame.getPlayer();
        this.spellManager = modeleGame.getSpellManager();
        this.monsterManager = modeleGame.getMonsterManager();
        this.boardManager = new BoardManager(scene);
        boardManager.addEntityToCanvas(player);
        monsterManager.addObserver(this);
        this.animationTimer = animTimer;

        this.playerWon = false;

        initColors();
    }

    /**
     * methode appelee quand on active la touche pour re-commencer le jeu
     * NOTA : si votre jeu se joue à l'infini, redéfinir cette fonction
     * en ne faisant rien
     */
    @Override
    public void reset() {

        refillLife();
        monsterManager.reset(boardManager);
        spellManager.flushSpellsFromGame(boardManager);
        player.reset();

    }

    /**
     * methode appelee a chaque tour de boucle
     * mise a jour des composants graphiques
     * à définir en fonction du modèles et des composants FXML
     */
    @Override
    public void render() {

        if (player.isGameOver()) {
            //LE JOUEUR A PERDU
            CsteJeu.isBoss = false;

            monsterManager.flushMonstersFromGame(boardManager);
            spellManager.flushSpellsFromGame(boardManager);
            checkPlayerLife();

            animationTimer.stop();

            Launcher.wave -= 2;

            if (Launcher.wave <= 0) {
                Launcher.isOver = true;
            } else {
                Launcher.shootPhase = true;
            }

            Launcher.launch(Launcher.LAUNCH_GAME);

        } else if (playerWon) {
            //LE JOUEUR A GAGNE
            CsteJeu.isBoss = false;

            monsterManager.flushMonstersFromGame(boardManager);
            spellManager.flushSpellsFromGame(boardManager);
            checkPlayerLife();

            if (isBoss) {
                Launcher.wave++;
            } else {
                Launcher.questionsPhase = true;
            }

            animationTimer.stop();
            Launcher.launch(Launcher.LAUNCH_GAME);
        } else {
            checkPlayerLife();
            updatePlayerPosition();
            spellManager.visualSpellUpdate(boardManager);
            player.updateTimer();
            if (CsteJeu.isBoss) {
                monsterManager.updateMonstersPositions(player, scene.getHeight());
                monsterManager.visualMonstersUpdate(boardManager, this.player);
                monsterManager.deleteMonstersFromGame(boardManager);
            } else {
                monsterManager.visualMonstersUpdate(boardManager, this.player);
                monsterManager.deleteMonstersFromGame(boardManager);
            }
        }
    }

    /**
     * Méthode permettant de mettre a jour la position du personnage
     * en fonction des entrées clavier effectuées par le joueur
     */
    private void updatePlayerPosition() {
        if (player.getTopPosition().y == 0 && up) {
            unUp();
        }
        if (player.isUnderGround(scene.getHeight() - GameController.LAYOUT_SIZE) && down) {
            unDown();
        }
        if (up) {
            player.moveUp();
        }
        if (down) {
            player.moveDown();
        }

        if (left) {
            setSelectedSpellLeft();
            player.switchPrevSpell(((GameModel) model).pathImagePlayer());
            unLeft();
        }

        if (right) {
            setSelectedSpellRight();
            player.switchNextSpell(((GameModel) model).pathImagePlayer());
            unRight();
        }
    }

    /**
     * méthode appelée pour associer les actions aux touches dans la scene devint
     * est un ensemble d'instructions de la forme
     * scene.mapKeyPressedToConsumer(KeyCode.<touche>, (x) -> <fonction>);
     * où <touche> est une touche du clavier et <fonction> est un appel de fonction du contrôle
     */
    @Override
    protected void mapTouchToActions() {
        scene.mapKeyPressedToConsumer(KeyCode.UP, (x) -> up());
        scene.mapKeyPressedToConsumer(KeyCode.DOWN, (x) -> down());
        scene.mapKeyReleasedToConsumer(KeyCode.UP, (x) -> unUp());
        scene.mapKeyReleasedToConsumer(KeyCode.DOWN, (x) -> unDown());
        scene.mapKeyPressedToConsumer(KeyCode.LEFT, (x) -> left());
        scene.mapKeyPressedToConsumer(KeyCode.RIGHT, (x) -> right());
        scene.mapKeyPressedToConsumer(KeyCode.SPACE, (x) -> fire());
        scene.mapKeyPressedToConsumer(KeyCode.R, (x) -> reset());
    }

    protected void refillLife() {
        heart1.setImage(FULL_HEART);
        heart2.setImage(FULL_HEART);
        heart3.setImage(FULL_HEART);
    }

    protected void checkPlayerLife() {
        if (2.5 == player.getLife()) {
            heart3.setImage(HALF_HEART);
        }
        if (2.0 == player.getLife()) {
            heart3.setImage(EMPTY_HEART);
        }
        if (1.5 == player.getLife()) {
            heart2.setImage(HALF_HEART);
        }
        if (1.0 == player.getLife()) {
            heart2.setImage(EMPTY_HEART);
        }
        if (0.5 == player.getLife()) {
            heart1.setImage(HALF_HEART);
        }
        if (0 == player.getLife()) {
            heart1.setImage(EMPTY_HEART);
        }
    }


    protected void initColors() {
        switch (GAMEDIFFICULTY) {
            case NORMAL:
                /*
                color1.setFill(natureType.get(0).getColor());
                color2.setFill(natureType.get(1).getColor());
                color3.setFill(natureType.get(2).getColor());
                color1.setStrokeWidth(15);
                color4.setVisible(false);
                */
                selectedSpell = 1;
                break;
            case HARD:
                /*
                color1.setFill(natureType.get(0).getColor());
                color2.setFill(natureType.get(1).getColor());
                color3.setFill(natureType.get(2).getColor());
                color4.setFill(natureType.get(3).getColor());
                color1.setStrokeWidth(15);
                */
                selectedSpell = 1;
                break;
            default:
                /*
                color2.setFill(natureType.get(0).getColor());
                color3.setFill(natureType.get(1).getColor());
                color2.setStrokeWidth(15);
                color1.setVisible(false);
                color4.setVisible(false);
                */
                selectedSpell = 2;
                break;
        }
    }

    protected void setSelectedSpellLeft() {
        if (selectedSpell == 1 && GAMEDIFFICULTY == DIFFICULTY.NORMAL) {
            /*
            color1.setStrokeWidth(1);
            color3.setStrokeWidth(15);
            */
            selectedSpell = 3;
            return;
        }
        if (selectedSpell == 1 && GAMEDIFFICULTY == DIFFICULTY.HARD) {
            /*
            color1.setStrokeWidth(1);
            color4.setStrokeWidth(15);
            */
            selectedSpell = 4;
            return;
        }
        if (selectedSpell == 2 && GAMEDIFFICULTY == DIFFICULTY.EASY) {
            /*
            color2.setStrokeWidth(1);
            color3.setStrokeWidth(15);
            */
            selectedSpell = 3;
            return;
        }
        if (selectedSpell == 2) {
            /*
            color2.setStrokeWidth(1);
            color1.setStrokeWidth(15);
            */
            selectedSpell = 1;
        }
        if (selectedSpell == 3) {
            /*
            color3.setStrokeWidth(1);
            color2.setStrokeWidth(15);
            */
            selectedSpell = 2;
        }
        if (selectedSpell == 4) {
            /*
            color4.setStrokeWidth(1);
            color3.setStrokeWidth(15);
            */
            selectedSpell = 3;
        }
    }

    protected void setSelectedSpellRight() {
        if (selectedSpell == 4) {
            /*
            color4.setStrokeWidth(1);
            color1.setStrokeWidth(15);
            */
            selectedSpell = 1;
            return;
        } else if (selectedSpell == 3 && GAMEDIFFICULTY == DIFFICULTY.NORMAL) {
            /*
            color3.setStrokeWidth(1);
            color1.setStrokeWidth(15);
            */
            selectedSpell = 1;
            return;
        } else if (selectedSpell == 3 && GAMEDIFFICULTY == DIFFICULTY.EASY) {
            /*
            color3.setStrokeWidth(1);
            color2.setStrokeWidth(15);
            */
            selectedSpell = 2;
            return;
        }
        if (selectedSpell == 3) {
            /*
            color3.setStrokeWidth(1);
            color4.setStrokeWidth(15);
            */
            selectedSpell = 4;
        }
        if (selectedSpell == 2) {
            /*
            color2.setStrokeWidth(1);
            color3.setStrokeWidth(15);
            selectedSpell = 3;
        }
        if (selectedSpell == 1) {
            /*
            color1.setStrokeWidth(1);
            color2.setStrokeWidth(15);
            */
            selectedSpell = 2;
        }

    }

    /**
     * Méthode appelée lorsque le joueur lance un sort avec le personnage
     */
    private void fire() {
        if (player.canFire()) {
            this.spellManager.addSpell(player.fireSpell(), this.boardManager);
            player.resetTimer();
        }
    }

    /**
     * Méthode appelée lorsque le joueur veut monter avec le personnage
     */
    private void up() {
        up = true;
    }

    /**
     * Méthode appelée lorsque le joueur veut monter avec le personnage
     */
    private void down() {
        down = true;
    }

    /**
     * Méthode appelée lorsque le joueur arrete de monter avec le personnage
     */
    private void unUp() {
        up = false;
    }

    /**
     * Méthode appelée lorsque le joueur arrete de monter avec le personnage
     */
    private void unDown() {
        down = false;
    }

    private void left() {
        left = true;
    }

    private void unRight() {
        right = false;
    }

    private void unLeft() {
        left = false;
    }

    private void right() {
        right = true;
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof MonsterManager) {
            scoreLabel.setText(String.valueOf(ScoreManager.score));
            //LE JOUEUR A GAGNE
            if ((int) arg <= 0) {
                playerWon = true;
            }
        }
    }

}