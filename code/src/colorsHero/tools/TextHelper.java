package colorsHero.tools;

import javafx.scene.control.TextField;

public class TextHelper {

    public static void backSpace(Object o) {
        if (o instanceof TextField) {
            TextField currTextField = (TextField) o;
            if (currTextField.getText().length() > 0) {
                int caretPosition = currTextField.getCaretPosition();
                currTextField.setText(currTextField.getText(0, currTextField.getText().length() - 1));
                currTextField.positionCaret(caretPosition - 1);
            }
        }
    }

    public static void caretLeft(Object o) {
        if (o instanceof TextField) {
            TextField currTextField = (TextField) o;
            if (currTextField.getText().length() > 0 && currTextField.getCaretPosition() > 0)
                currTextField.positionCaret(currTextField.getCaretPosition() - 1);
        }
    }

    public static void caretRight(Object o) {
        if (o instanceof TextField) {
            TextField currTextField = (TextField) o;
            if (currTextField.getText().length() > 0 && currTextField.getCaretPosition() < currTextField.getText().length())
                currTextField.positionCaret(currTextField.getCaretPosition() + 1);
        }
    }

}
